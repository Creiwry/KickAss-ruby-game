# Kickass - Terminal Zombie Game

Welcome to Kickass, the Terminal Zombie Game! This is an exciting, turn-based, terminal game where you, the player, must survive an onslaught of zombies. 

## Getting Started

### Prerequisites

Make sure you have installed:
- Ruby 3.1.3 


### Installing

1. Clone the repository:
    ```
    git clone https://github.com/Creiwry/KickAss-ruby-game.git
    ```

2. Navigate into the project directory:
    ```
    cd KickAss-ruby-game

4. Run bundle install:
    ```
   bundle install 
    ```

5. Run the game:
    ```
    ruby app.rb
    ```

### Trial Versions (initial versions to learn concepts)
The simplest game version trial is called app_1.rb
  ```
    ruby app_1.rb
  ```
The slightly more complex trial version is called app_2.rb
  ```
    ruby app_2.rb
  ```

## How to Play

1. At the start of the game, you will be asked to enter your character's name.

2. You will then be presented with a number of options:
    - **a** - Search for a new weapon. This could improve your chances of survival!
    - **s** - Search for a health pack. This can heal you up and prolong your life!
    - **x** - Attack an enemy in sight. Get those zombies!
    - **0** - Exit the game. Only for the faint-hearted.

3. The game keeps track of the number of zombies left and the number of zombies in sight.

4. Your goal is to survive all zombie attacks. The game continues until you either defeat all the zombies or get defeated yourself.

