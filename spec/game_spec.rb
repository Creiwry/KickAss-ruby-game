# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/game'

RSpec.describe 'Game object' do
  subject(:game) { Game.new('Mikasa') }

  it 'has a human player' do
    expect(game.human_player).to be_a(HumanPlayer)
    expect(game.human_player.name).to eq('Mikasa')
  end

  describe '#kill_player' do
    before do
      enemy_one = Player.new('enemy_1')
      enemy_two = Player.new('enemy_2')
      game.enemies_in_sight << enemy_one
      game.enemies_in_sight << enemy_two
    end

    it 'removes a player from enemies array' do
      enemy_two = game.enemies_in_sight.last
      game.kill_player(enemy_two)
      expect(game.enemies_in_sight).not_to include(enemy_two)
    end
  end

  it 'has players_left counter' do
    expect(game.players_left).to eq(10)
  end

  it 'has enemies_in_sight array' do
    expect(game.enemies_in_sight).to be_an(Array)
    expect(game.enemies_in_sight).to be_empty
  end

  describe '#is_still_ongoing?' do
    it 'returns true when the game is ongoing' do
      enemy = proc { Player.new('enemy') }
      allow(game).to receive(:enemies_in_sight).and_return([enemy, enemy])
      expect(game.is_still_ongoing?).to be true
    end

    it 'returns false when the game is over' do
      allow(game).to receive(:players_left).and_return(0)
      allow(game.human_player).to receive(:life_points).and_return(0)
      expect(game.is_still_ongoing?).to be false
    end

    it 'returns false when the human player is the last one alive' do
      game.players_left = 0
      expect(game.is_still_ongoing?).to be false
    end
  end

  describe '#show_players' do
    it 'displays the state of the human player and the number of enemies' do
      expect { game.show_players }
        .to output(/Mikasa has \d+ life points and a weapon of level \d+./)
        .to_stdout
      expect { game.show_players }
        .to output(/There are \d+ enemies left./)
        .to_stdout
      expect { game.show_players }
        .to output(/There are \d+ enemies in sight./)
        .to_stdout
    end
  end

  describe '#menu' do
    it 'displays the menu' do
      allow(game).to receive(:gets).and_return('a')
      expect { game.menu }
        .to output(/What action do you want to perform?/)
        .to_stdout
    end
  end

  describe '#menu_choice' do
    # TODO: add other menu options
    it 'reacts to user choice' do
      allow(game).to receive(:gets).and_return('a')
      allow(game.human_player).to receive(:search_weapon)

      game.menu

      expect(game.human_player).to have_received(:search_weapon)
    end
  end

  describe '#enemies_attack' do
    it 'all living enemies retaliate' do
      game.enemies_in_sight.each do |enemy|
        expect(enemy).to receive(:attacks).with(game.human_player)
      end
      game.enemies_attack
    end
  end

  describe '#new_players_in_sight' do
    context 'when all players are in sight' do
      before do
        enemy_one = Player.new('enemy_1')
        enemy_two = Player.new('enemy_2')
        game.enemies_in_sight << enemy_one
        game.enemies_in_sight << enemy_two
      end

      it 'does not add any new players' do
        game.players_left = game.enemies_in_sight.length
        expect { game.new_players_in_sight }.to output(/All players are already in view/).to_stdout
        expect(game.enemies_in_sight.size).to eq(2)
      end
    end

    context 'when roll is 1' do
      it 'does not add any new players' do
        allow(game).to receive(:roll_die).and_return(1)
        expect { game.new_players_in_sight }.to output(/No new players this turn/).to_stdout
        expect(game.enemies_in_sight.size).to eq(0)
      end
    end

    context 'when roll is between 2 and 4' do
      it 'adds one new player' do
        allow(game).to receive(:roll_die).and_return(2)
        expect { game.new_players_in_sight }.to output(/A new player is in sight!/).to_stdout
        expect(game.enemies_in_sight.size).to eq(1)
      end
    end

    context 'when roll is 5 or 6' do
      it 'adds two new players' do
        allow(game).to receive(:roll_die).and_return(5)
        expect { game.new_players_in_sight }.to output(/Two new players are in sight!/).to_stdout
        expect(game.enemies_in_sight.size).to eq(2)
      end
    end
  end

  describe '#end' do
    context 'player has won' do
      it 'displays an end game message' do
        game.enemies_in_sight = []
        expect { game.end }.to output(/Congratulations, you have won!/).to_stdout
      end
    end

    context 'player has lost' do
      it 'displays an end game message' do
        game.human_player.life_points = 0
        expect { game.end }.to output(/You have lost. Better luck next time!/).to_stdout
      end
    end
  end
end
