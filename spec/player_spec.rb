# frozen_string_literal: true

require 'spec_helper'
require_relative '../lib/player'

RSpec.describe 'player object' do
  it 'has a name' do
    player_one = Player.new('Armin')
    expect(player_one.name).to eq('Armin')
  end

  describe '#show_state' do
    it 'shows state of a player' do
      player_one = Player.new('Armin')
      expect { player_one.show_state }.to output(/Armin has 20 life points./).to_stdout
    end
  end

  describe 'player health' do
    it 'has initial level of 20' do
      player_one = Player.new('Armin')
      expect(player_one.life_points).to eq(20)
    end
  end

  describe '#gets_damage' do
    it 'decreases when player dealt damage' do
      player_one = Player.new('Armin')
      player_one.gets_damage(5)
      expect(player_one.life_points).to eq(15)
    end

    it 'causes player death when reaches 0' do
      player_one = Player.new('Armin')
      expect { player_one.gets_damage(20) }.to output(/Armin is dead/).to_stdout
      expect { player_one.show_state }.to output(/Armin has 0 life points./).to_stdout
    end

    it 'shows player death when damage is higher than life points' do
      player_two = Player.new('Eren')
      expect { player_two.gets_damage(32) }.to output(/Eren is dead/).to_stdout
      expect { player_two.show_state }.to output(/Eren has 0 life points./).to_stdout
    end
  end

  describe '#attacks' do
    it 'returns attack announcement' do
      player_one = Player.new('Armin')
      player_two = Player.new('Eren')
      allow(player_two).to receive(:gets_damage).and_return(4)
      expect { player_two.attacks(player_one) }
        .to output(/Eren has dealt Armin \d+ damage/)
        .to_stdout
    end

    it 'causes opponent to lose health' do
      player_one = Player.new('Armin')
      player_two = Player.new('Eren')
      player_two_life_points = player_two.life_points
      player_one.attacks(player_two)
      expect(player_two.life_points).to be < player_two_life_points
    end
  end
end

RSpec.describe 'human player object' do
  let(:human_player_one) { HumanPlayer.new('Armin') }
  let(:human_player_two) { HumanPlayer.new('Mikasa') }

  it 'has a name' do
    expect(human_player_one.name).to eq('Armin')
  end

  it 'has a weapon' do
    expect(human_player_one.weapon_level).to eq(1)
  end

  describe '#show_state' do
    it 'shows state of a player' do
      expect { human_player_one.show_state }.to output(/Armin has 100 life points and a weapon of level 1./).to_stdout
    end
  end

  describe 'player health' do
    it 'has initial level of 100' do
      expect(human_player_one.life_points).to eq(100)
    end
  end

  describe '#search weapon' do
    before do
      human_player_two.weapon_level = 3
    end

    it 'prints a message stating the new weapon level' do
      allow(human_player_two).to receive(:roll_die).and_return(5)
      expect { human_player_two.search_weapon }.to output(/You found a weapon of level 5./)
        .to_stdout
    end

    it 'finds a new weapon and keeps it if it has a higher level' do
      allow(human_player_two).to receive(:roll_die).and_return(4)
      expect { human_player_two.search_weapon }
        .to output(/It's better than your current weapon: you take it./)
        .to_stdout
      expect(human_player_two.weapon_level).to eq(4)
    end

    it 'finds a new weapon and discards it if it has a lower or equal level' do
      allow(human_player_two).to receive(:roll_die).and_return(3)
      expect { human_player_two.search_weapon }
        .to output(/It is not better than your current weapon: you discard it./)
        .to_stdout
      expect(human_player_two.weapon_level).to eq(3)
    end
  end

  describe '#search_health_pack' do
    before do
      human_player_two.weapon_level = 3
      human_player_two.life_points = 50
    end

    it 'finds no health pack if the roll is 1' do
      allow(human_player_two).to receive(:roll_die).and_return(1)
      expect { human_player_two.search_health_pack }
        .to output(/You found nothing.../).to_stdout
      expect(human_player_two.life_points).to eq(50)
    end

    it 'finds a 50 points health pack if the roll is between 2 and 5' do
      allow(human_player_two).to receive(:roll_die).and_return(4)
      expect { human_player_two.search_health_pack }
        .to output(/Congratulations, you have found a pack of \+10 life points!/).to_stdout
      expect(human_player_two.life_points).to eq(60)
    end

    it 'finds an 80 points health pack if the roll is 6' do
      human_player_two.life_points = 10
      allow(human_player_two).to receive(:roll_die).and_return(6)
      expect { human_player_two.search_health_pack }
        .to output(/Wow, you found a pack of \+20 life points!/).to_stdout
      expect(human_player_two.life_points).to eq(30)
    end
  end
end
