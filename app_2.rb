# frozen_string_literal: true

require 'bundler'
Bundler.require

require_relative 'lib/game'
require_relative 'lib/player'

33.times { print '-'.colorize(:blue) }
print "\n|".colorize(:blue)
print '       Welcome to Kickass!     '.colorize(:red)
print "|\n".colorize(:blue)
print '|'.colorize(:blue)
print '  The last one standing wins!  '.colorize(:red)
print "|\n".colorize(:blue)
33.times { print '-'.colorize(:blue) }
puts ''
puts 'Your fighters are here!'
puts ''
print 'Set player name > '

user_input = gets.chomp
player1 = HumanPlayer.new(user_input)
player2 = Player.new('Josiane')
player3 = Player.new('Jose')
enemies = [player2, player3]
puts ''

while player1.life_points.positive? && (player2.life_points.positive? || player3.life_points.positive?)
  player1.show_state
  puts "\nWhat action do you want to perform?"
  puts 'a - Search for a new weapon'.colorize(:yellow)
  puts 's - Search for a health pack'.colorize(:green)
  puts "\nAttack an enemy:"
  player2.show_state
  player3.show_state
  puts "0 - attack #{player2.name}".colorize(:red)
  puts "1 - attack #{player3.name}".colorize(:red)
  user_input = gets.chomp

  case user_input
  when 'a'
    player1.search_weapon
  when 's'
    player1.search_health_pack
  when '0'
    player1.attacks(player2)
  when '1'
    player1.attacks(player3)
  else
    puts 'wrong input'
  end

  if player2.life_points.positive? || player3.life_points.positive?
    puts 'Enemies are attacking!'
    enemies.each do |enemy|
      enemy.attacks(player1) if enemy.life_points.positive?
      break if player1.life_points <= 0
    end
  end

  break if player1.life_points <= 0
end

puts 'Game is over'
if player1.life_points.positive?
  puts 'Congratulations, you won!'
else
  puts 'You lost, try again'
end
