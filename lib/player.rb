# frozen_string_literal: true

require 'colorize'

class Player
  attr_accessor :name, :life_points

  def initialize(name)
    @name = name
    @life_points = 20
  end

  def gets_damage(damage)
    @life_points -= damage
    if @life_points <= 0
      @life_points = 0
      puts "#{name} is dead".colorize(:light_red)
    else
      show_state
    end
  end

  def attacks(player)
    # puts "#{name} is attacking #{player.name}."
    damage = roll_die
    puts "#{name} has dealt #{player.name} #{damage} damage.".colorize(:light_red)
    player.gets_damage(damage)
  end

  def show_state
    puts "#{name} has #{life_points} life points.".colorize(:light_red)
  end

  private

  def roll_die
    rand(1..6)
  end
end

class HumanPlayer < Player
  attr_accessor :weapon_level

  def initialize(name)
    super(name)
    @life_points = 100
    @weapon_level = 1
  end

  def show_state
    puts "#{name} has #{life_points} life points and a weapon of level #{weapon_level}.".colorize(:light_blue)
  end

  def search_weapon
    new_weapon = roll_die
    puts "You found a weapon of level #{new_weapon}."

    if new_weapon > weapon_level
      @weapon_level = new_weapon
      print_weapon
      puts "It's better than your current weapon: you take it.".colorize(:yellow)
    else
      puts 'It is not better than your current weapon: you discard it.'
    end
  end

  def attacks(player)
    puts "#{name} is attacking #{player.name}.".colorize(:light_blue)
    damage = roll_die * weapon_level
    player.gets_damage(damage)
    puts "#{name} has dealt #{player.name} #{damage} damage.".colorize(:light_blue)
  end

  def gets_damage(damage)
    @life_points -= damage
    return unless @life_points <= 0

    @life_points = 0
    puts "#{name} is dead".colorize(:light_red)
  end

  def search_health_pack
    chance = roll_die

    if chance == 1
      puts 'You found nothing...'
    elsif chance == 6
      change_health(20)
      print_life_pack(:green)
      puts 'Wow, you found a pack of +20 life points!'.colorize(:green)
    else
      change_health(10)
      print_life_pack(:cyan)
      puts 'Congratulations, you have found a pack of +10 life points!'.colorize(:cyan)
    end
  end

  private

  def change_health(health)
    if health > (100 - life_points)
      @life_points = 100
    else
      @life_points += health
    end
  end

  def print_weapon
    puts '      /| ________________'.colorize(color: :yellow, mode: :bold)
    puts 'O|===|* >________________>'.colorize(color: :yellow, mode: :bold)
    puts '      \|'.colorize(color: :yellow, mode: :bold)
  end

  def print_life_pack(color)
    puts ' +---+'.colorize(color:, mode: :bold)
    puts ' | + |'.colorize(color:, mode: :bold)
    puts ' +---+'.colorize(color:, mode: :bold)
  end
end
