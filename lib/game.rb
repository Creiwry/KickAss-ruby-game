# frozen_string_literal: true

require_relative './player'
require 'colorize'

class Game
  attr_accessor :human_player, :enemies_in_sight, :players_left

  def initialize(human_player)
    @human_player = HumanPlayer.new(human_player)
    @players_left = 10
    @enemies_in_sight = []
  end

  def menu
    user_input = nil
    loop do
      puts "\nWhat action do you want to perform?"
      puts 'a - Search for a new weapon'.colorize(:yellow)
      puts 's - Search for a health pack'.colorize(:green)
      puts 'x - Attack an enemy in sight'.colorize(:red)
      puts '0 - Exit the game'
      user_input = gets.chomp
      break if %w[a s x 0].include?(user_input)

      puts 'wrong input'
    end
    menu_choice(user_input)
  end

  def execute_turn
    while @enemies_in_sight.empty?
      puts 'There are no enemies in sight! Calling new players...'
      new_players_in_sight
      print_players_and_enemies
    end
    menu
    enemies_attack
    show_players
  end

  def show_players
    puts "#{@human_player.name} has #{@human_player.life_points} life points and a weapon of level #{@human_player.weapon_level}.".colorize(:light_blue)
    puts "There are #{@players_left} enemies left.".colorize(:light_red)
    puts "There are #{@enemies_in_sight.length} enemies in sight.".colorize(:light_red)
    print_players_and_enemies
  end

  def menu_choice(input)
    case input
    when 'a'
      @human_player.search_weapon
    when 's'
      @human_player.search_health_pack
    when 'x'
      enemy = @enemies_in_sight.sample
      @human_player.attacks(enemy)
      kill_player(enemy) if enemy.life_points <= 0
    when '0'
      puts 'Exiting the game ...'
      exit(0)
    else
      puts 'wrong input'
      menu
    end
  end

  def is_still_ongoing?
    @human_player.life_points.positive? && @players_left.positive?
  end

  def new_players_in_sight
    if @players_left == @enemies_in_sight.length
      puts 'All players are already in view'
      return
    end

    chance = roll_die

    if chance == 1
      puts 'No new players this turn'
    elsif chance <= 4 || (@players_left - @enemies_in_sight.length) == 1
      create_enemy
      puts 'A new player is in sight!'.colorize(:red)
    else
      2.times do
        create_enemy
      end
      puts 'Two new players are in sight!'.colorize(:red)
    end
  end

  def kill_player(player)
    @enemies_in_sight.delete(player)
    @players_left -= 1
  end

  def enemies_attack
    return unless @enemies_in_sight.any?

    puts 'Enemies are attacking!'.colorize(:red)

    @enemies_in_sight.each do |enemy|
      enemy.attacks(@human_player)
    end
  end

  def end
    if @human_player.life_points.positive?
      puts 'Congratulations, you have won!'.colorize(:blue)
      print_player_celebrating
    else
      puts 'You have lost. Better luck next time!'.colorize(:red)
      print_player_dead
    end
  end

  private

  def roll_die
    rand(1..6)
  end

  def create_enemy
    new_player = Player.new("zombie_#{rand(42..420)}")
    @enemies_in_sight << new_player
  end

  def print_enemy(color)
    [
      '  O '.colorize(color:, mode: :bold),
      ' -|-'.colorize(color:, mode: :bold),
      ' / \\'.colorize(color:, mode: :bold)
    ]
  end

  def print_player
    [
      '  o_ '.colorize(color:  :blue, mode: :bold),
      ' <|  '.colorize(color:  :blue, mode: :bold),
      ' / } '.colorize(color:  :blue, mode: :bold)
    ]
  end

  def print_player_celebrating
    puts ' \ o / '.colorize(color:  :blue, mode: :bold)
    puts '   |   '.colorize(color:  :blue, mode: :bold)
    puts '  / \  '.colorize(color:  :blue, mode: :bold)
  end

  def print_player_dead
    puts '  \__/o  '.colorize(color:  :red, mode: :bold)
    puts '  /  \   '.colorize(color:  :red, mode: :bold)
  end

  def print_players_and_enemies
    lines = [
      player_line(1),
      '     ',
      enemy_line(@enemies_in_sight.length, :red),
      enemy_line(@players_left - @enemies_in_sight.length)
    ].flatten(1)

    puts ''
    puts " #{@human_player.name}".colorize(color: :blue, mode: :bold)
    3.times do |i|
      lines.each do |line|
        print line[i]
      end
      puts ''
    end

    if @enemies_in_sight.any?
      print "  #{@human_player.life_points}".colorize(color: :blue, mode: :bold)
      print '   '
      @enemies_in_sight.each do |enemy|
        print "#{enemy.life_points}  ".colorize(color: :red, mode: :bold)
      end
    else
      print " #{@human_player.life_points}".colorize(color: :blue, mode: :bold)
    end
    puts ''
    puts "  #{@human_player.weapon_level}".colorize(color: :yellow, mode: :bold)
  end

  def player_line(count)
    Array.new(count) { print_player }
  end

  def enemy_line(count, color = :magenta)
    Array.new(count) { print_enemy(color) }
  end
end
