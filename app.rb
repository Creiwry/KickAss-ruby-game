# frozen_string_literal: true

require 'bundler'
Bundler.require

require_relative 'lib/game'
require_relative 'lib/player'

33.times { print '-'.colorize(:blue) }
print "\n|".colorize(:blue)
print '       Welcome to Kickass!     '.colorize(:red)
print "|\n".colorize(:blue)
print '|'.colorize(:blue)
print '  The last one standing wins!  '.colorize(:red)
print "|\n".colorize(:blue)
33.times { print '-'.colorize(:blue) }
puts ''
print 'Set player name > '

user_input = gets.chomp
my_game = Game.new(user_input)

my_game.new_players_in_sight
my_game.show_players
my_game.execute_turn while my_game.is_still_ongoing?

my_game.end
