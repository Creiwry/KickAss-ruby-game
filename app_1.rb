# frozen_string_literal: true

require 'bundler'
Bundler.require

require_relative 'lib/game'
require_relative 'lib/player'

player1 = Player.new('Josiane')
player2 = Player.new('Jose')

33.times { print '-'.colorize(:blue) }
print "\n|".colorize(:blue)
print '       Welcome to Kickass!     '.colorize(:red)
print "|\n".colorize(:blue)
print '|'.colorize(:blue)
print '  The last one standing wins!  '.colorize(:red)
print "|\n".colorize(:blue)
33.times { print '-'.colorize(:blue) }
puts ''
puts 'Your fighters are here!'
puts ''
player1.show_state
player2.show_state
puts ''

while player1.life_points.positive? && player2.life_points.positive?
  player1.attacks(player2)
  puts ''
  break if player2.life_points <= 0

  player2.attacks(player1)
  puts ''
end
